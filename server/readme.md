# web-shop
- Setup and instalation
- Featres
- Documentation


### Setup and instalation
- clone the git repository
- start the server:
    - cd server
    - npm install
    - setup the database:
        - input the database information inside .env
        - cd setup
        - npm create_database.js
        - npm create_table.js
        - npm import_products.js 
    - npm server.js

- start frontend app:
    - cd client/web_shop_client
    - npm install
    - npm dev

### Frontend Features
- SignUp (/signUp)
- SignIn (/signIn)
- View Products (/)
- View extended (/products/:id) by clicking on the item on the home page or on the Cart page.