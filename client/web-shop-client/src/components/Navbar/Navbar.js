"use client"
import React, { useContext, useEffect, useRef, useState } from "react";
import NavbarMobile from "./NavbarMobile";
import NavbarDesktop from "./NavbarDesktop";
import { UserProvider } from "@/context/UserContext";
import { CartProvider } from "@/context/CartContext";

const NavBar = () => {
    const [isScrolled, setIsScrolled] = useState(false);
    const [isMobile, setIsMobile] = useState(null);

    useEffect(() => {   // to handle which navbar to show
        const handleResize = () => {
            setIsMobile(window.innerWidth <= 1024);
        };

        handleResize();

        window.addEventListener('resize', handleResize);

        return () => window.removeEventListener('resize', handleResize);
    }, []);

    useEffect(() => {   // to add shadow under the navbar, when user scrolls the page
        const handleScroll = () => {
            const isUserScrolled = window.scrollY > 50;
            setIsScrolled(isUserScrolled);
        };
        window.addEventListener('scroll', handleScroll);
        return () => window.removeEventListener('scroll', handleScroll);
    }, []);


    return (

            <div className={`fixed top-0 left-0 right-0 z-[9999999] transition-all ease-in-out duration-300 ${isScrolled ? 'shadow-lg' : ''}`}>
                {isMobile ? <NavbarMobile /> : <NavbarDesktop />}
            </div>
        );
};

export default NavBar;
