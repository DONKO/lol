import { render } from '@testing-library/react'
import { useRouter } from 'next/navigation'
import NavbarDesktop from './NavbarDesktop';
import { UserProvider } from '@/context/UserContext';
import { CartProvider } from '@/context/CartContext';
import { act } from 'react-dom/test-utils';


global.fetch = jest.fn(() =>
  Promise.resolve({
    json: () => Promise.resolve({ rates: {id: 1}}),
  })
);

jest.spyOn(console, 'error').mockImplementation(jest.fn());

beforeEach(() => {
  fetch.mockClear();
  jest.clearAllMocks();
});


// Mock useRouter:
jest.mock("next/navigation", () => ({

    usePathname(){
        return '/signIn'
    },

    useRouter() {
      return {
        prefetch: () => null
      };
    }
  }));

 
it('renders correctly', () => {
  act(() => {
      const { container } = render(<CartProvider><UserProvider><NavbarDesktop /></UserProvider></CartProvider>)
      expect(container).toMatchSnapshot()
  });
})
